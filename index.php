<?php
$thankQ = 0;
$inValidEmail = 0;
if (isset($_POST) && isset($_POST['email'])) {
    $email_id = $_POST['email'];
    if (filter_var($email_id, FILTER_VALIDATE_EMAIL)) {
        $user = "root";
        $pass = "123";
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=vt_electronix', $user, $pass);
        } catch (Exception $ex) {
            die("Unable to connect: " . $e->getMessage());
        }

        try {
            $stat = $dbh->prepare("INSERT INTO notification_subscription (id, name, email_id, created_at, updated_at) 
          values (NULL, NULL, :email_id, NOW(), NOW())");
            $stat->bindParam(':email_id', $email_id);
            $stat->execute();
            $thankQ = 1;
        } catch (Exception $ex) {
            
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>VT Electronix Coming Soon</title>
        <link href="tools/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="tools/jquery.min.js"></script> 
        <script type="text/javascript" src="tools/cufon-yui.js"></script>
        <script type="text/javascript" src="tools/Abraham_Lincoln_400.font.js"></script>
        <script type="text/javascript" src="tools/Inspiration_400.font.js"></script>
        <script type="text/javascript" src="tools/Museo_Slab_100_400-Museo_Slab_700_400.font.js"></script>
        <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript">
            Cufon.replace('.logo h1', {fontFamily: 'Inspiration'});
            Cufon.replace('.logo h2', {fontFamily: 'Museo Slab 100'});
            Cufon.replace('.logo h2 span', {fontFamily: 'Abraham Lincoln'});
            Cufon.replace('.form span', {fontFamily: 'Museo Slab 700'});
            Cufon.replace('p.big_text, p.small_text', {fontFamily: 'Museo Slab 100'});
            Cufon.replace('p.big_text strong, p.small_text strong', {fontFamily: 'Museo Slab 700'});
            $(document).ready(function () {
                $('#email').keypress(function () {
                    var email = $('#email').val();
                    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                    if (re.test(email)) {
                        $('#email-invalid').css('display', 'none');
                    } else {
                        $('#email-invalid').css('display', 'block');
                    }
                });
            });
            function validateEmail() {
                var email = $('#email').val();
                var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                var return_value = false;
                if (re.test(email)) {
                    $('#email-invalid').css('display', 'none');
                    return_value = true;
                } else {
                    $('#email-invalid').css('display', 'block');
                }
                return return_value;
            }
        </script>
    </head>
    <body>
        <div id="transy">
        </div>
        <div id="wrapper">
            <div class="logo">
                <h1>VT Electronix</h1>
                <h2>
                    <strong class="one"></strong>design <span>&amp;</span> development of Eletronics Stuff<strong class="two"></strong>
                </h2>
            </div>
            <div class="content">
                <p class="big_text"><strong>We are working on something</strong> very interesting!</p>
                <p class="small_text"><strong>be notified.</strong> We just need your email address.</p>
                <form action="#" method="post" onsubmit=" return validateEmail();">
                    <div class="form">
                        <div class="field_content" style=" margin-bottom: 10px;">                        
                            <input class="field" type="email"  name="email" id="email" placeholder="Enter email address" />
                            <input class="submit" type="submit" value="" />
                        </div>
                    </div>
                </form>

                <div class="form" style=" width: 550px; text-align: center;">
                    <?php if ($thankQ) { ?>
                        <span style=" color: greenyellow;">*Successfully Subscribed to VT Electronix. Hope to see you soon..</span> 
                    <?php } ?>
                    <span style=" color: red; display: none;" id="email-invalid">*Please enter a valid email.</span> 
                </div>

                <div class="clear"></div>
                <ul class="social">
                    <!--                    <li class="pinterest"><a href="#"></a></li>-->
                    <li class="instagram"><a href="https://www.instagram.com/its_vt/"></a></li>
                    <li class="twitter"><a href="https://twitter.com/vishwateja2000"></a></li>
                    <li class="facebook"><a href="https://www.facebook.com/vishwateja.j"></a></li>
                </ul>
            </div>
            <div class="clear" style="height: 25px;"></div>
            <hr></hr>
            <footer style='text-align: center;'>
                <p>&copy; 2005 - <?php echo date('Y') ?> by <a href="http://vishwateja.com/"  target="_blank" style="text-decoration-line: none; font-style: italic;">Vishwa Teja</a></p>
            </footer>
        </div>

    </body>
</html>
